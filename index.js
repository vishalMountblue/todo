const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const path = require('path')
app.use(bodyParser.json());

const { Client } = require("pg");
const client = new Client({
  host: "localhost",
  user: "postgres",
  port: 5432,
  password: "root@123",
  database: "todo",
});

client
  .connect()
  .then(() => console.log("Database connected"))
  .catch("Error in connecting to database");

  app.get('/',(req,res)=>{
    res.sendFile(path.join(__dirname,'public','introduction.html'))
  })

app.get("/create-todo-table", (req, res) => {
  let createTableQuery = `create table if not exists "tasks" (id serial, text varchar(1000), "isCompleted" boolean not null default false);`;
  client.query(createTableQuery, (err, result) => {
    if (err) {
      res.send(err);
      console.error("Error in creating tables ", err);
    } else {
      res.send("Table created");
    }
  });
});

app.post("/todos", async (req, res) => {
  try {
    const insertResponse = await client.query(
      `insert into tasks (text,"isCompleted") values ($1,$2) returning *`,
      [req.body.text, req.body.isCompleted]
    );
    console.log(insertResponse);
    res.send(insertResponse.rows);
  } catch (err) {
    console.log(err);
    res.sendStatus(400);
  }
});

app.get("/todos", async (req, res) => {
  try {
    const getAllPostResponse = await client.query("select * from tasks");
    res.send(getAllPostResponse.rows);
  } catch (err) {
    console.error(err);
    res.send(404);
  }
});

app.get("/todos/:id", async (req, res) => {
  try {
    const getAllPostResponse = await client.query(
      `select * from tasks where id = ${req.params.id}`
    );
    if (getAllPostResponse.rows.length === 0) {
      res.send({ message: "not found" });
    } else res.send(getAllPostResponse.rows);
  } catch (err) {
    console.error(err);
    res.sendStatus(404);
  }
});

app.put("/todos/:id", async (req, res) => {
  try {
    const updatePostResponse = await client.query(
      `update tasks set text = $1, "isCompleted" = $2 where id = ${req.params.id} returning *`,
      [req.body.text, req.body.isCompleted]
    );
    res.send(updatePostResponse.rows);
  } catch (err) {
    console.error(err);
    res.sendStatus(400);
  }
});

app.delete("/todos/:id", async (req, res) => {
  try {
    const deletePostResponse = await client.query(
      `delete from tasks where id = ${req.params.id} `
    );
    res.send(deletePostResponse);
  } catch (err) {
    console.error(err);
    res.sendStatus(404);
  }
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
